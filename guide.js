Session s50
	Create a s50-s55 folder
		Inside the folder run, npx create-react-app react-app
			Wait for the "Happy hacking!" sign
		cd react-app/
			touch guide.js
			subl .
			npm start, - opens a React App browser in the google chrome with localhost: 3000 in the gitbash server
			remove unneccessary files:
				In src folder:
					App.test.js
					index.css
					logo.svg
					reportWebVitals.js
				in index.js :
					remove the corresponding links to above 4
				in app.js:
					delete the import logo from './logo.svg' files
			install bootstrap dependencies
				npm install react-bootstrap bootstrap
			create a "components" folder in src
				create AppNavbar.js
				create Banner.js
				create Highlights.js
				refactor App.css
			create a page folder in src
				create Home.js
			Activity s50
				Create CourseCard.js
Session s51
	Create a data folder inside src folder
	Create a Courses.js in pages
	inside react-app:
	
	npm install  "--save styled-components" for scroll styling 