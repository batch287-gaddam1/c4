import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import AdminProductView from './components/AdminProductView';
import Scroll from './components/Scroll';
import Footer from './components/Footer';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Help from './pages/Help';
import Notifications from './pages/Notifications';
import DownloadApp from './pages/DownloadApp';
import Cart from './pages/Cart';
import Admin from './pages/Admin';
import AdminInterface from './pages/AdminInterface';
import AddProduct from './pages/AddProduct';
import UpdateProduct from './pages/UpdateProduct';
// import Highlights from './components/Highlights';
import './App.css';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

export default function App() {

  // State hook for the user state that's defined here is for a global scope.
  // To identify whether a user is already login or not, by means of localStorage.
  // const [ user, setUser ] = useState({
  //   email: localStorage.getItem('email')
  // });

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  console.log(user);

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {

    // fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    fetch('http://localhost:4004/users/details', {
      methos:'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [] )

  return (
    // A common pattern in React for the component to return multiple elements : <> and </>
    <>
    <UserProvider value={{ user, setUser, unsetUser }} >
      <Router>
        <AppNavbar/>
        <Scroll/>
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/adminLogin" element={<Admin />} />
            <Route path="/adminInterface" element={<AdminInterface />} />
            <Route path="/register" element={<Register />} />
            <Route path="/help" element={<Help />} />
            <Route path="/notifications" element={<Notifications />} />
            <Route path="/downloadApp" element={<DownloadApp />} />
            <Route path="/products/addproducts" element={<AddProduct />} />
            <Route path="/products/updateProduct" element={<UpdateProduct />} />
            <Route path='/*' element={<Error />} />
            {/*<Home />*/}
            {/*<Courses />*/}
            {/*<Register />*/}
            {/*<Login />*/}
          </Routes>
        </Container>
        <Footer/>
      </Router>
    </UserProvider>
    </>
  );
}

// export default App;