import { Row, Col } from 'react-bootstrap';

export default function Footer() {

	return(
		<Row className="footer bg-info p-5 " variant="bottom">
		<Col className="mt-2 mb-2">
			<h4 className="bg-info col" style={{color: "rgba(255,255,0,0.6)", textAlign: ""}}> About </h4>
			<h5> E-Commerce platform </h5>
		</Col>
		<Col className="mt-2 mb-2">
			<h4 className="bg-info col"> Reach us </h4>
			<h5> Contact us at ecommerce@gmail.com </h5>
		</Col>
		<Col className="mt-2 mb-2">
			<h4 className="bg-info col"> Address </h4>
			<h5 className="col-4" align=""> ABC, Near ABC Road, Opposite ABC ,Pincode 500xxx </h5>
		</Col>
		
		</Row>
	)
}