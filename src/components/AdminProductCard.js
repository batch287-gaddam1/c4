import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import UpdateProduct from '../pages/UpdateProduct';

export default function AdminProductCard({product}) {

const { name, description, price, isActive, _id } = product;

const navigate = useNavigate();
// const [ count, setCount ] = useState(0);
// const [ seats, setSeats ] = useState(30);
// const [ isOpen, setIsOpen ] = useState(true);

// function archive() {
// 	if(seats > 0){
// 		setCount(count + 1);
// 		console.log('Enrollees: ' + count)
// 		setSeats(seats - 1);
// 		console.log('Seats: ' + seats)
// 	} 
// 	else {
// 		console.log("No more seats!");
// 	};
// }

// useEffect(() => {
// 	if(seats === 0){
// 		setIsOpen(false);
// 		alert("No More Seats!");
// 		document.querySelector(`#btn-archive-${_id}`).setAttribute('disabled', true);
// 	}
// })

function archive(e) {
	// e.preventDefault();
	
	// Code to empty the input field for name, description, price after clicking on Add Product button.
	
	// setName("");
	// setDescription("");
	// setPrice("");

	
	// alert("Product Added");


	

	// fetch(`http://localhost:4004/products/archive/${_id}`, {
	fetch(`https://capstone-2-gaddam.onrender.com/products/archive/${_id}`, {
	    method: "PATCH",
	    headers: {
	        
	        Authorization: `Bearer ${localStorage.getItem('token')}`		    },
	    
	})
	.then(res => res.json())
	.then(data => {

	    console.log(data);

	    if(data === true){

	        // Clear input fields
	        // setName('');
	        // setDescription('');
	        
	        // setPrice('');
	        

	        Swal.fire({
	            title: 'Product Archived Successfully',
	            icon: 'success',
	            text: 'Product Archive'
	        });

	        // Allows us to redirect the user to the admin Interface page after archiving a product
	        navigate("/adminInterface");

	    } else {

	        Swal.fire({
	            title: 'Something wrong',
	            icon: 'error',
	            text: 'Please try again.'   
	         });

	    };
	})


};

useEffect(() => {
		// alert("Product Archived!");
		// document.querySelector(`#btn-archive-${_id}`).setAttribute('disabled', true);
	
})

function activate(e) {
	// e.preventDefault();
	
	// Code to empty the input field for name, description, price after clicking on Add Product button.
	
	// setName("");
	// setDescription("");
	// setPrice("");

	
	// alert("Product Added");


	

	// fetch(`http://localhost:4004/products/activate/${_id}`, {
	fetch(`https://capstone-2-gaddam.onrender.com/products/activate/${_id}`, {
	    method: "PUT",
	    headers: {
	        
	        Authorization: `Bearer ${localStorage.getItem('token')}`		    },
	    
	})
	.then(res => res.json())
	.then(data => {

	    console.log(data);

	    if(data === true){

	        // Clear input fields
	        // setName('');
	        // setDescription('');
	        
	        // setPrice('');
	        

	        Swal.fire({
	            title: 'Product Activated Successfully',
	            icon: 'success',
	            text: 'Product Activation'
	        });

	        // Allows us to redirect the user to the admin Interface page after activating a product
	        navigate("/adminInterface");

	    } else {

	        Swal.fire({
	            title: 'Something wrong',
	            icon: 'error',
	            text: 'Please try again.'   
	         });

	    };
	})


};

return(
	// <Row className="mt-3 mb-3">
	// 				<Col xs={12}>
	// 					<Card className="cardHighlight p-0">
	// 						<Card.Body>
	// 							<Card.Title><h4>{name}</h4></Card.Title>
	// 							<Card.Subtitle>Description:</Card.Subtitle>
	// 							<Card.Text>{description}</Card.Text>
	// 							<Card.Subtitle>Price:</Card.Subtitle>
	// 							<Card.Text>{price}</Card.Text>
	// 							<Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
	// 						</Card.Body>
	// 					</Card>
	// 				</Col>
	// 	</Row>

	<Row className="mt-3 mb-3">
	<Card className="text-center mb-2">
      <Card.Header>Featured</Card.Header>
      <Card.Body>
        <Card.Title><h4>{name}</h4></Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Card.Text>
          Reviews
        </Card.Text>
        

        { 
        (isActive === true) ?
        <>
        <Button className="bg-secondary m-2" onClick={archive} id={`btn-archive-${_id}`}>Archive</Button>

        <Button className="bg-primary m-2" as={Link} to={`/products/updateProduct`}>Update</Button>
        </>
        :
        <>
        <Button className="bg-primary m-2" onClick={activate} id={`btn-activate-${_id}`}>Activate</Button>
        
        <Button className="bg-primary m-2" as={Link} to={`/products/updateProduct`}>Update</Button>
        </>
        }

      </Card.Body>
      <Card.Footer className="text-muted">2 days ago</Card.Footer>
    </Card>
    
    </Row>
	)
};