import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {useParams} from 'react-router-dom';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import {useState,useEffect} from 'react';

function CartCard(product) {

  const { name, description, price, _id } = product;

  // const {productId} = useParams();
  // console.log(productId);
  // const {productName} = useParams();
  // console.log(productName)

  // const {productId} = useParams();

  // const [name, setName] = useState("");
  // const [description, setDescription] = useState("");
  // const [price, setPrice] = useState(0);
  const buyNow = () => {
    Swal.fire({
      title: "Order Placed Successfully",
      icon: "success",
      text: "You have successfully placed the order"
    })
  }

  // useEffect(() => {
  //   console.log(productId);

  //   fetch(`http://localhost:4004/products/${productId}`)
  //   .then(res => res.json())
  //   .then(data => {
  //     console.log(data);
  //     setName(data.name);
  //     setDescription(data.description);
  //     setPrice(data.price);
  //   })
  // }, [productId]);

  return (
    <Card>
      <Card.Header as="h5">Featured</Card.Header>
      <Card.Body>
        
        <Card.Title><h4>{name}</h4></Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Button className="bg-primary" onClick={() => buyNow()}>Buy Now</Button>
      </Card.Body>
    </Card>
  );
}

export default CartCard;