// My Activity code

// import { Row, Col, Card, Button } from 'react-bootstrap';

// export default function CourseCard() {
// 	return (
// 		<Row className="mt-3 mb-3">
// 		    <Col xs={12} >
// 		        <Card className="CourseCard p-3">
// 		        <Col>
// 		        	<Card.Title>
// 		        	    <h2>Sample Course</h2>
// 		        	</Card.Title>
// 		            <Card.Body>
// 		                <Card.Title>
// 		                    <h4>Description:</h4>
// 		                </Card.Title>
// 		                <Card.Text>
// 		                	This is a sample Course
// 		                </Card.Text>
// 		                <Card.Title>
// 		                    <h4>Price:</h4>
// 		                </Card.Title>
// 		                <Card.Text>
// 		                	40000 Rs
// 		                </Card.Text>
// 		            </Card.Body>
// 		        </Col>
// 		        <Col className="p-3">
// 		            <Button variant="primary"> Enroll </Button>
// 		        </Col>
// 		        </Card>
// 		    </Col>
// 		</Row>
// 	)
// };

// Prof. code

import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function ProductCard({product}) {

const { name, description, price, _id } = product;

// React Hook that lets you add a state variable to the components.
// Use the state hook for this component to be able to store its state
// States are used to keep track of information related to individual components

// Syntax:
	// const [ getter, setter ] = useState(initialGetterValue)

// State Hook
// const [ count, setCount ] = useState(0);
// const [ seats, setSeats ] = useState(30);
// const [ isOpen, setIsOpen ] = useState(true);
// console.log(useState(0));

function addToCart() {
	// my activity code
	// if(count <= 29){//restricting the maximum count to 30.Can also use (count < 30).
	// 	setCount(count + 1)
	// } else {
	// 	alert("No more seats!");
	// };

	// if(count >= 1){
	// 	setCount(count - 1)
	// } else {
	// 	alert("No more Seats!")
	// };
	// Both the above are correct, first one with useState(0) goes from 0 to 30 and the second one with useState(30) goes from 30 to 0

	// Prof. Code
	// if(seats > 0){
	// 	setCount(count + 1);
	// 	console.log('Enrollees: ' + count)
	// 	setSeats(seats - 1);
	// 	console.log('Seats: ' + seats)
	// } 
	// else {
	// 	alert("No more seats!")
	// };
	
};
	
	// useEffect - allows us to instruct the app that the component needs to do something
	// Keyword for useEffect, "reactive" . It makes our app, reactive.

	// useEffect(() => {
	// 	if(seats === 0){
	// 		setIsOpen(false);
	// 		alert("No More Seats!");
	// 		document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 	}
	// }, [ seats ] )//here [ seats ] is the dependencies in the useEffect like/as the seats in the if(seats === 0). whatever in the if(), needs to be at the end like seats here.Its an optional feature, i.e., we may or may not write this.

	return (
		// <Row className="mt-3 mb-3">
		// 	<Col xs={12}>{/*xs: for extra small screens */}
		// 		<Card className="cardHighlight p-0">{/*in react.js class is className*/}
		// 			<Card.Body>
		// 				<Card.Title><h4>{name}</h4></Card.Title>
		// 				<Card.Subtitle>Description</Card.Subtitle>
		// 				<Card.Text>{description}</Card.Text>
		// 				<Card.Subtitle>Price:</Card.Subtitle>
		// 				<Card.Text>{price}</Card.Text>
		// 				<Card.Subtitle>Enrolless:</Card.Subtitle>
		// 				<Card.Text>{count} Enrollees</Card.Text>
		// 				<Card.Subtitle>Seats Available:</Card.Subtitle>
		// 				<Card.Text>Available Seats: {seats}</Card.Text>
		// 				<Button id={`btn-enroll-${id}`} className="bg-primary" onClick={enroll}>Enroll</Button>
		// 			</Card.Body>
		// 		</Card>
		// 	</Col>
		// </Row>

		<Row className="mt-3 mb-3">
					<Col xs={12}>
						<Card className="cardHighlight p-0">
							<Card.Body>
								<Card.Title><h4>{name}</h4></Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>{price}</Card.Text>
								<Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
							</Card.Body>
						</Card>
					</Col>
		</Row>

	)
};