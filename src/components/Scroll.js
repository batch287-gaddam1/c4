import { Row, Col } from 'react-bootstrap';
import styled from 'styled-components';

export default function Scroll(){
  return (
    <ScrollStyle>
    <Row className='marquee'>
          <Col className='marqueeContentContainer'>
            <p> <b>Sale Begins Early at 12:00 AM </b> </p>
            
          </Col>
    </Row>
    </ScrollStyle>
  )
}

const ScrollStyle = styled.section`
.marquee {
    height: 100%;
    width:100%;
    overflow: hidden;
    position: relative;
}

.marqueeContentContainer {
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content: space-evenly;
    height: 100%;
    -moz-animation: scroll-left 20s linear infinite;
    -webkit-animation: scroll-left 20s linear infinite;
    animation: scroll-left 20s linear infinite;
    color: #fb7b61;
}

@keyframes scroll-left {
    0% {
        transform: translate(100%, 0);
    }
    100% {
        transform: translate(-100%, 0);
    }
}
`;