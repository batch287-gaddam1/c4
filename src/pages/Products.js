// import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductCard';
import { useState, useEffect } from 'react';

export default function Products() {

	const [ products, setProducts ] = useState([]);

	// console.log(coursesData);
	// console.log(coursesData[0]);

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course={course} />
	// 	);
	// });

	useEffect(() => {
		// fetch(`${process.env.REACT_APP_API_URL}/products/active-products`)
		// fetch(`http://localhost:4004/products/active-products`)
		fetch(`https://capstone-2-gaddam.onrender.com/products/active-products`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setProducts(data.map(product => {
					return (
						<ProductCard key={product._id} product={product} />
					)
				}))
			})
	}, [])

	return(
		<>
		{/*<h1>Products</h1>*/}
		{/*<ProductCard product={productsData[0]} />*/}
		{products}
		</>
	);
};