import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';

export default function Home() {

	const data = {
		title: 'E-Commerce Platform',
		content: "Shopping",
		destination: "/products",
		label: "Shop Now!"
	};

	return(
		<>
			<Banner data={data} />
			<Highlights />
			{/*<CourseCard />*/}{/*removed this to add the coursecard in the courses tab*/}
		</>
	)
};