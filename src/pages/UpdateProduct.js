import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
// import product from '../components/AdminProductCard';

export default function UpdateProduct(product) {

	// const {_id} = product;
	// console.log(product)
	// const { user, setUser } = useContext(UserContext); //My code
	const { user } = useContext(UserContext);//Prof. code

	const navigate = useNavigate();
	
	const {productId} = useParams();

	console.log(productId);
	const {productName} = useParams();

	// State hooks to store the values of the input fields.
	// const [ email, setEmail ] = useState('');
	// const [ password1, setPassword1 ] = useState('');
	// const [ password2, setPassword2 ] = useState('');
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState('');

	// State to determine whether submit button is enabled or not.
	const [ isActive, setIsActive ] = useState();

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match.
		if(name !== "" || description !== "" || price !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ name, description, price]);

	// Check if values are successfully binded
	    // console.log(name);
	    // console.log(description);
	    // console.log(price);

	

	function updateProduct(e) {
		e.preventDefault();
		
		// Code to empty the input field for name, description, price after clicking on Add Product button.
		
		setName("");
		setDescription("");
		setPrice("");

		
		// alert("Product Added");
		Swal.fire({
		    title: 'Product Updated Successfully',
		    icon: 'success',
		    text: 'Product Updated'
		});
	

		

		fetch(`http://localhost:4004/products/${productId}`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json',
		        Authorization: `Bearer ${localStorage.getItem('token')}`		    },
		    body: JSON.stringify({
		        name: name,
		        description: description,
		        price: price
		    	})
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){

		        // Clear input fields
		        setName('');
		        setDescription('');
		        
		        setPrice('');
		        

		        Swal.fire({
		            title: 'Product Updated Successfully',
		            icon: 'success',
		            text: 'Product Updated'
		        });

		        // Allows us to redirect the user to the admin Interface page after adding a product
		        navigate("/adminInterface");

		    } else {

		        Swal.fire({
		            title: 'Something wrong',
		            icon: 'error',
		            text: 'Please try again.'   
		         });

		    };
		})

	
	};

	return (
		// (user.isAdmin !== false) ?
		// // (user.id !== undefined) ?
		// 	<Navigate to="/addproducts" />
		// :
			<Form onSubmit={(e) => updateProduct(e)}>
				<h1>Update Product</h1>
				<Form.Group controlId="name">
					<Form.Label>Product Name: {productName}</Form.Label>
					<Form.Control
						type="String"
						placeholder="name"
						value={name}
						onChange={e => setName(e.target.value)}
						
					/>
					
				</Form.Group>

				<Form.Group controlId="description">
					<Form.Label>Description</Form.Label>
					<Form.Control
						type="String"
						placeholder="description"
						value={description}
						onChange={e => setDescription(e.target.value)}
						
					/>
					
				</Form.Group>

				

				<Form.Group controlId="price">
					<Form.Label>Price</Form.Label>
					<Form.Control
						type="number"
						placeholder={price}
						value={price}
						onChange={e => setPrice(e.target.value)}
						
					/>
				</Form.Group>

				

				{
					isActive ?
						<Button variant="primary my-3" type="submit" id="submitBtn">
							Update Product
						</Button>
						:
						<Button variant="danger my-3" type="submit" id="submitBtn" disabled>
							Update Product
						</Button>
				}

				
			</Form>
	)
}