// import { Row, Col, Button } from 'react-bootstrap';
import Banner from '../components/Banner';

export default function error() {

	const data = {
		title: "Error 404 - Page Not Found!",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}
	return (
		<Banner data={data} />

		// <Row>
		// 	<Col>
		// 		<h1>Error 404 - Page Not Found!</h1>
		// 		<p>Go back to <b>homepage</b>.</p>
		// 		<Button variant="primary"> Back to Home </Button>
		// 	</Col>
		// </Row>
	)
}