
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AdminProductCard from '../components/AdminProductCard';
import { useState, useEffect } from 'react';

export default function Products() {

  const [ products, setProducts ] = useState([]);

  // console.log(coursesData);
  // console.log(coursesData[0]);

  // const courses = coursesData.map(course => {
  //  return (
  //    <CourseCard key={course.id} course={course} />
  //  );
  // });

  useEffect(() => {
    // fetch(`${process.env.REACT_APP_API_URL}/products/active-products`)
    // fetch(`http://localhost:4004/products/all`)
    fetch(`https://capstone-2-gaddam.onrender.com/products/all`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setProducts(data.map(product => {
          return (
            <AdminProductCard key={product._id} product={product} />
          )
        }))
      })
  }, [])

  return(
    <>
    {/*<h1>Products</h1>*/}
    {/*<ProductCard product={productsData[0]} />*/}
    {products}
    <Button className="bg-primary col-md-4 offset-md-4 offset-4 mb-4" as={Link} to={`/products/addproducts`}>Add Product(s)</Button>
    </>
  );
};