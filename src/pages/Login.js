import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(props) {

	// Allows us to consume the User Context object and it's properties to use for user validation.
	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();
	
	// State hooks to store the values of the input fields.
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	// State to determine whether submit button is enabled or not.Vaidation for useEffect
	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match.
		if(email !== "" && password !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})

	const retrieveUserDetails = (token) => {

		// fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		// fetch('http://localhost:4004/users/details', {
		fetch('https://capstone-2-gaddam.onrender.com/users/details', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Global user state for validation across the whole project
			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used  for validation across the whole application.
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	function userlogin(e) {
		e.preventDefault();
	
		// setEmail("");
		// setPassword("");

		// Sets the email of the authenticated/login user in the local storage.
		// localStorage.setItem('email', email);

		// Sets the global user state to have properties obtained from the local storage.
		// setUser({email: localStorage.getItem("email")});

		// alert(`"${email}" has been verified! Welcome Back!`);

		// navigate("/");//Once the user login, the page will be redirected to route "/" i.e., here its home page

		// fetch('http://localhost:4004/users/login', {
		fetch('https://capstone-2-gaddam.onrender.com/users/login', {
		// fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to E-Commerce!"
				})

				// setEmail('');
				// setPassword('');
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please check your login details and try again!"
				})
			}
		})

		setEmail('');
		setPassword('');
	};

	return (

		// (user.email !== null) ?
		(user.id !== null)  ?
			<Navigate to="/products" />
		:
			<Form onSubmit={(e) => userlogin(e)}>
			<h1>Login Page</h1>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter Password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
					<Form.Text muted>
						Password must contains letters, numbers, special characters, it must be 8-20 characters long
					</Form.Text>
				</Form.Group>

				{
					isActive ?
						<Button variant="primary my-3" type="submit" id="submitBtn">
							Submit
						</Button>
						:
						<Button variant="danger my-3" type="submit" id="submitBtn" disabled>
							Submit
						</Button>
				}
				<p>To <b> <NavLink to="/adminLogin" >Login</NavLink> </b> as Admin</p>
			</Form>
		
	)
}