import { Row } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import CartCard from '../components/CartCard';
import { useEffect, useState } from 'react';
// import Nav from 'react-bootstrap';

export default function Cart() {

	const { user } = useContext(UserContext);
	console.log(user);

	const [ cartProducts, setCartProducts ] = useState([]);

	// useEffect(() => {
	// 	fetch('http://localhost:4004/users/checkOut', {
	// 	    method: 'POST',
	// 	    headers: {
		        
	// 	        Authorization: `Bearer ${localStorage.getItem('token')}`		    },
		    
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {

	// 	    console.log(data);
	// }, [])

	useEffect(() => {
		// fetch(`${process.env.REACT_APP_API_URL}/products/active-products`)
		// fetch(`http://localhost:4004/users/myOrders`,{
		fetch(`https://capstone-2-gaddam.onrender.com/users/myOrders`,{
			headers: {
		        Authorization: `Bearer ${localStorage.getItem('token')}`		    },
		})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setCartProducts(data.map(cart => {
					return (
						<CartCard key={cart._id} cart={cart} />
					)
				}))
			})
	}, [])



	return(
		<Row>
			<h1> Your Cart Items </h1>
			{(user.id !== null) ?
			<>
			<p> Cart was open </p>
			{cartProducts}
			</>
			:
			<p> Kindly <b> <NavLink to="/login" >Login</NavLink> </b> to show the cart</p>
			}
		</Row>
	)
}


// In Postman
// checkOut old token (create order)
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YjdhNmNhNjUxYjkxODVhNjEzM2ZiOSIsImVtYWlsIjoiYWRtaW5AbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2ODk4MzEwNjB9.DlioPQpiDc4Khb1ahmrBLl4yBBi9026Ge4WeTHE-D1s

// check user details old token
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YzBmNGFiM2NiYzdlMGFkZmQ3MmFkZCIsImVtYWlsIjoiYWRtaW4xMkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2OTMyOTQ4ODF9.2twYi34pmcmIjGFYET9SRTNPdvskw_mubNUkQBCqdCY