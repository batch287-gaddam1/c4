import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

	// const { user, setUser } = useContext(UserContext); //My code
	const { user } = useContext(UserContext);//Prof. code

	const navigate = useNavigate();
	
	// State hooks to store the values of the input fields.
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	// const [ firstName, setFirstName ] = useState('');
	// const [ lastName, setLastName ] = useState('');
	// const [ mobileNo, setMobileNo ] = useState('');

	// State to determine whether submit button is enabled or not.
	const [ isActive, setIsActive ] = useState();

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match.
		if((email !== "" && password1 !== "" && password2 !== "") && (password2 === password1)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ email, password1, password2]);

	// Check if values are successfully binded
	    // console.log(email);
	    // console.log(password1);
	    // console.log(password2);

	

	function registerUser(e) {
		e.preventDefault();
		
		// Code to empty the input field for email, password1, password2 after clicking on submit button.
		// setEmail("");
		// setPassword1("");
		// setPassword2("");
		// setFirstName("");
		// setLastName("");
		// setMobileNo("");

		// setUser({email: localStorage.getItem("email")});//No need of this.This is required in the login code, not in register code.

		// alert("Thank you for registering!");
	// };

		// fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		// fetch(`http://localhost:4004/users/checkEmail`, {
		fetch(`https://capstone-2-gaddam.onrender.com/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				

				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email!"
				})

				// setEmail('');
				// setPassword('');
			} else {
				//fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
				// fetch('http://localhost:4004/users/register', {
				fetch('https://capstone-2-gaddam.onrender.com/users/register', {
				    method: "POST",
				    headers: {
				        'Content-Type': 'application/json'
				    },
				    body: JSON.stringify({
				        // firstName: firstName,
				        // lastName: lastName,
				        email: email,
				        // mobileNo: mobileNo,
				        password: password1
				    	})
				})
				.then(res => res.json())
				.then(data => {

				    console.log(data);

				    if(data === true){

				        // Clear input fields
				        // setFirstName('');
				        // setLastName('');
				        setEmail('');
				        // setMobileNo('');
				        setPassword1('');
				        setPassword2('');

				        Swal.fire({
				            title: 'Registration successful',
				            icon: 'success',
				            text: 'Welcome to Zuitt!'
				        });

				        // Allows us to redirect the user to the login page after registering for an account
				        navigate("/login");

				    } else {

				        Swal.fire({
				            title: 'Something wrong',
				            icon: 'error',
				            text: 'Please try again.'   
				         });

				    };
				})
			};
		})

		// setEmail('');
		// setPassword1('');
		// setPassword2("");
		// setFirstName("");
		// setLastName("");
		// setMobileNo("");
	};

	return (
		// (user.email !== null) ?
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
			<Form onSubmit={(e) => registerUser(e)}>
				<h1>Registration Page</h1>
				{/*<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="String"
						placeholder="Enter First Name here"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
					
				</Form.Group>

				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="String"
						placeholder="Enter Last Name here"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
					
				</Form.Group>*/}

				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				{/*<Form.Group controlId="mobileNumber">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="Number"
						placeholder="Enter Your Mobile Number here"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}
						required
					/>
					
				</Form.Group>*/}

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{
					isActive ?
						<Button variant="primary my-3" type="submit" id="submitBtn">
							Submit
						</Button>
						:
						<Button variant="danger my-3" type="submit" id="submitBtn" disabled>
							Submit
						</Button>
				}

				{/*<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>*/}

			</Form>
	)
}
